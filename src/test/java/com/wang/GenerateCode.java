package com.wang;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

public class GenerateCode {
    public static void main(String[] args) {
        String url = "jdbc:mysql:///prodb";
        String username = "root";
        String password = "abc123";
        String moduleName = "sys";
        String mapperLocation = "C:\\Users\\24017\\Desktop\\小项目\\项目后端\\oneProjectBack\\src\\main\\resources\\mapper\\" + moduleName;
        String tables = "user";
        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> {
                    builder.author("wang") // 设置作者
                            .outputDir("C:\\Users\\24017\\Desktop\\小项目\\项目后端\\oneProjectBack\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.wang") // 设置父包名
                            .moduleName(moduleName) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, mapperLocation)); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tables) // 设置需要生成的表名
                            .addTablePrefix(); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
