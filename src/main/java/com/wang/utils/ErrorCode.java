package com.wang.utils;

public enum ErrorCode {


    SUCCESS_CODE(20000, "成功"),
    NULL_ERROR(20001, "请求数据为空"),
    PARAMS_ERROR(20002, "请求参数错误"),
    Other_ERROR(20003, "");
    private final int code;
    private final String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
