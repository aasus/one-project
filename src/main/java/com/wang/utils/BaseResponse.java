package com.wang.utils;

import lombok.Data;

@Data
public class BaseResponse<T> {
    private String message;
    private T data;
    private int code;


    public BaseResponse(T data) {
        this.code = ErrorCode.SUCCESS_CODE.getCode();
        this.message = ErrorCode.SUCCESS_CODE.getMessage();
        this.data = data;
    }

    public BaseResponse(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
        this.data = null;
    }

    public BaseResponse(ErrorCode errorCode, String message) {
        this.code = errorCode.getCode();
        this.message = message;
        this.data = null;
    }

    public BaseResponse() {

    }
}
