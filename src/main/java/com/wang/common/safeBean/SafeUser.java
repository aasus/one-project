package com.wang.common.safeBean;

import com.wang.sys.entity.User;

public class SafeUser {
    public static User getSafeUser(User user) {
        User safeUser = new User();
        safeUser.setId(user.getId());
        safeUser.setName(user.getName());
        safeUser.setAge(user.getAge());
        safeUser.setEmail(user.getEmail());
        safeUser.setIdCard(user.getIdCard());
        return safeUser;
    }
}
