package com.wang.common.requestBean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUser {
     private String name;
     private String userPassword;
     private int IdCard;

}
