package com.wang.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wang.common.requestBean.RegisterUser;
import com.wang.common.safeBean.SafeUser;
import com.wang.sys.entity.User;
import com.wang.common.requestBean.LoginUser;
import com.wang.sys.service.UserService;
import com.wang.utils.BaseResponse;
import com.wang.utils.ErrorCode;
import com.wang.utils.ResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wang
 * @since 2023-04-16
 */
@Api("用户接口")
@RestController
@RequestMapping("/user")
public class UserController {
    public static final String SALT_VALUE = "wang";
    @Resource
    public UserService service;

    @GetMapping("/all")
    @ApiOperation(value = "查询所有")
    public BaseResponse<List<User>> getAll() {
        List<User> list = service.list(null);
        return ResultUtils.success(list);
    }

    @PostMapping("/login")
    @ApiOperation(value = "用户登入")
    public BaseResponse<User> userLogin(@RequestBody LoginUser user) {
        if (user == null) {
            return ResultUtils.error(ErrorCode.NULL_ERROR);
        }
        //查询用户存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        user.setUserPassword(DigestUtils.md5DigestAsHex(user.getUserPassword().getBytes()));
        queryWrapper.eq("name", user.getName());
        queryWrapper.eq("userPassword", user.getUserPassword());
        User loginUser = service.getOne(queryWrapper);
        if (loginUser == null) {
            return ResultUtils.error(ErrorCode.Other_ERROR, "未查询用户");
        }
        return ResultUtils.success(SafeUser.getSafeUser(loginUser));

    }

    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public BaseResponse<String> userLogin(@RequestBody RegisterUser registerUser) {
        if (ObjectUtils.isEmpty(registerUser) && registerUser.getIdCard() == 0) {
            return ResultUtils.error(ErrorCode.PARAMS_ERROR);
        }
        //查询用户存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", registerUser.getName());
        List<User> list = service.list(queryWrapper);
        if (list.size() > 0) {
            return ResultUtils.error(ErrorCode.Other_ERROR, "用户已存在");
        }
//        加密
        String md5Password = DigestUtils.md5DigestAsHex(registerUser.getUserPassword().getBytes());
        User user = new User();
        user.setName(registerUser.getName());
        user.setAge(0);
        user.setEmail("");
        user.setUserPassword(md5Password);
        user.setIsDeleted(0);
        user.setIdCard(registerUser.getIdCard());
        service.save(user);
        return ResultUtils.success("注册成功");

    }
}
