package com.wang.sys.service;

import com.wang.sys.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wang
 * @since 2023-04-16
 */
public interface UserService extends IService<User> {

}
