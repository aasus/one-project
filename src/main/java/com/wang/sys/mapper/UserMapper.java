package com.wang.sys.mapper;

import com.wang.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-04-16
 */
public interface UserMapper extends BaseMapper<User> {

}
